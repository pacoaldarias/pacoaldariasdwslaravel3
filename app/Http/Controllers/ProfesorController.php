<?php

// app/Http/Controllers/ProfesorCOntroller.php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Profesor;

class ProfesorController extends Controller {

    //

    public function index() {


        $profesores = Profesor::get();

        return view('profesores.index')->with('profesores', $profesores);
    }

    public function create() {
        return view('profesores.create');
    }

    public function store(Request $request) {

        $profesor = new Profesor;

        $max = Profesor::get()->max('id');
        $profesor->id = $max + 1;
        $profesor->nombre = $request->input('nombre');
        $profesor->save();

        return redirect()->route('profesores.index');
    }

    public function edit($id) {
        $profesor = Profesor::find($id);
        return view('profesores.edit')->with('profesor', $profesor);
    }

    public function update(Request $request, $id) {
        $profesor = Profesor::find($id);
        $profesor->id = $id;
        $profesor->nombre = $request->input('nombre');
        $profesor->save();
        return redirect()->route('profesores.index');
    }

    public function destroy($id) {
        Profesor::destroy($id);
        return redirect()->route('profesores.index');
    }

}
