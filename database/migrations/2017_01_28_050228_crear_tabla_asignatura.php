<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAsignatura extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('asignatura', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 80);
            $table->integer(horas);
            $table->integer(idp);
            $table->foreign('idp')->references('id')->on('profesor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('asignatura');
    }

}
