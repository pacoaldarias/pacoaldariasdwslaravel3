<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProfesorSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        for ($i = 0; $i < 50; $i++) {
            \DB::table('profesor')->insert(array(
                'id' => $i,
                'nombre' => 'profesor' . $i,
            ));
        }
    }

}
