<?php

use Illuminate\Database\Seeder;

class AsignaturaSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //

        for ($i = 0; $i < 50; $i++) {
            \DB::table('asignatura')->insert(array(
                'id' => $i,
                'nombre' => 'asignatura' . $i,
                'horas' => $i,
                'idp' => $i,
            ));
        }
    }

}
