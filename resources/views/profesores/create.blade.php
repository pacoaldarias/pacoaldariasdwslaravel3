@extends('profesores')

@section('content')

{!! Form::open([ 'route' => 'profesores.store', 'method' => 'POST']) !!}
@include('profesores.partials.fields')

<button type="submit" class="btn btn-success btn-block">Guardar</button>

{!! Form::close() !!}

@endsection