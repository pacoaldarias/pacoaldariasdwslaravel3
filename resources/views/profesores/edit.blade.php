@extends('profesores')

@section('content')

<h4 class="text-center">Editar Profesor: {{ $profesor->nombre  }}</h4>

{!! Form::model($profesor, [ 'route' => ['profesores.update', $profesor], 'method' => 'PUT']) !!}

@include('profesores.partials.fields')

<button type="submit" class="btn btn-success btn-block">Guardar cambios</button>

{!! Form::close() !!}

@endsection

