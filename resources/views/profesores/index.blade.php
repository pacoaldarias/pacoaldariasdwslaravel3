

@extends('profesores')

@section('content')

<a class="btn btn-success pull-right" href="{{ url('/profesores/create') }}" role="button">Nuevo profesor</a>

@include('profesores.partials.table')

@endsection

